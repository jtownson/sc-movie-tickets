import scala.collection.mutable.ListBuffer

class MovieTickets {

  val purchases: ListBuffer[(Int, Boolean)] = ListBuffer()

  def startPurchase(): Unit = purchases.clear()

  def addTicket(age: Int, isStudent: Boolean): Unit = purchases.append((age, isStudent))

  def finishPurchase(): Int =
    try {
      if (purchases.size < 20)
        purchases.map(price).sum
      else
        purchases.map(discountPrice).sum
    } finally {
      purchases.clear()
    }

  private def price(purchase: (Int, Boolean)): Int = purchase match { case(age, isStudent) =>
    if (age < 13) 550
    else if (age < 65 && !isStudent) 1100
    else if (age < 65 && isStudent) 800
    else 600
  }

  private def discountPrice(purchase: (Int, Boolean)): Int = purchase match { case(age, isStudent) =>
    if (age < 13) 550
    else 600
  }
}
