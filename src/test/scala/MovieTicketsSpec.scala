import org.scalatest.{FlatSpec, Matchers}

import org.scalatest.prop.TableDrivenPropertyChecks._

class MovieTicketsSpec extends FlatSpec with Matchers {

  val sampleData = Table(
    ("people", "expected price"),

    (List((25, false)), 1100),
    (List((18, true)), 800),

    (List((65, false)), 600),
    (List((65, true)), 600),

    (List((13, false)), 1100),
    (List((12, false)), 550),

    (List((25, false), (18, true)), 1100+800)
  )

  val movieTickets = new MovieTickets

  "Movie tickets" should "not add until start purchase is called" in {
    movieTickets.addTicket(age = 25, isStudent = false)
    movieTickets.startPurchase()
    movieTickets.finishPurchase() should be (0)
  }

  "Movie tickets" should "not double charge" in {
    movieTickets.startPurchase()
    movieTickets.addTicket(age = 25, isStudent = false)
    movieTickets.finishPurchase() should be (1100)
    movieTickets.finishPurchase() should be (0)
  }

  "The movie tickets" should "be priced right" in {
    forAll(sampleData) { (people: List[(Int, Boolean)], expectedPrice: Int) => {
      movieTickets.startPurchase()
      people.foreach(person => {
        val (age, isStudent) = person
        movieTickets.addTicket(age, isStudent)
      })
      movieTickets.finishPurchase() should be(expectedPrice)
    }
    }
  }

  "A group of 20 or more without children" should "charge $6.00 per person" in {
    val people = List.fill(20)(25, true)

    movieTickets.startPurchase()
    people.foreach(person => movieTickets.addTicket(person._1, person._2))

    movieTickets.finishPurchase() should be (20*600)
  }

  "A group of 20 or more children" should "charge $5.50 per person" in {
    val people = List.fill(20)(6, true)

    movieTickets.startPurchase()
    people.foreach(person => movieTickets.addTicket(person._1, person._2))

    movieTickets.finishPurchase() should be (20*550)
  }
}
